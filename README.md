# TextLocalizedTool
 Simple Text Localization Tools

## Installation 
For the installation you can use "Add Package from git URL". For more information you can use the link below:
>https://docs.unity3d.com/2019.3/Documentation/Manual/upm-ui-giturl.html

## What is it?
Text Localized Tool is a tool pacakge that help developers to make localization in their game. This tool has custom windows editor to help edit dictionary key localization. Text Localized use .csv file to write localization data and have 2 mode (online mode and offline mode) based on .csv file location.

For more information and example, download the sample project in the "Samples" folder.

## Feature
- Local and cloud .csv file location
- Add, edit and remove value by key in localised file (Offline mode only).
- Add, and remove language in localised file (Offline mode only).
- Create new localised file.
- Only show active language in .csv file (Hide language don't use) in localiser window and localization manager.
- Easy to use.
- 100 languages support.

## Languages support
Pleace check the link below to see the supported languages and their key code.
> https://cloud.google.com/translate/docs/languages

## Contributor
[Anas Rasyid](https://github.com/anasrasyid) 

## License
MIT
